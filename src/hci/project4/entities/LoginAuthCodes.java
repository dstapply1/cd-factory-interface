package hci.project4.entities;

public enum LoginAuthCodes {
	LOGIN_FAILED,
	LOGIN_SUCCESS,
	LOGIN_INVALID_EMAIL,
	LOGIN_INVALID_PASSWORD;
}
