package hci.project4.entities;

import java.util.ArrayList;

public class UserController {
	private static UserController instance = null;
	private ArrayList<User> registeredUsers;
	private User loggedInUser;

	public static UserController getInstance() {
		if (instance == null) {
			instance = new UserController();
		}
		
		return instance;
	}
	
	protected UserController() {
		registeredUsers = new ArrayList<User>();
		loggedInUser = null;
	}
	
	public ArrayList<User> getRegisteredUsers() {
		return registeredUsers;
	}
	
	public User getLoggedInUser() {
		return loggedInUser;
	}
	
	public boolean isUserRegistered(String email) {
		for (int i = 0; i < registeredUsers.size(); i++) {
			if (registeredUsers.get(i).getEmail().equals(email)) {
				return true;
			}
		}
		return false;
	}
	
	private User getCurrentUser(String email) {
		for (int i = 0; i < registeredUsers.size(); i++) {
			if (registeredUsers.get(i).getEmail().equals(email)) {
				return registeredUsers.get(i);
			}
		}
		return null;
	}
	
	// wouldn't normally pass password unhashed but lazy
	public LoginAuthCodes logUserIn(String email, String password) {
		if (isUserRegistered(email)) {
			User currentUser = getCurrentUser(email);
			if (currentUser.getPassword().equals(password)) {
				//User successfully logged in
				loggedInUser = currentUser;
				return LoginAuthCodes.LOGIN_SUCCESS;
			} else {
				// invalid password
				return LoginAuthCodes.LOGIN_INVALID_PASSWORD;
			}
		} else {
			// invalid email
			return LoginAuthCodes.LOGIN_INVALID_EMAIL;
		}
	}
	
	public void unLogUser() {
		loggedInUser = null;
	}
	
	public boolean isUserLoggedIn(String email) {
		if (isAUserLoggedIn() && loggedInUser.getEmail().equals(email)) {
			return true;
		}
		return false;
	}
	
	public boolean isAUserLoggedIn() {
		if (loggedInUser != null) {
			return true;
		}
		return false;
	}
	
	public void registerUser(User user) {
		registeredUsers.add(user);
	}
}
