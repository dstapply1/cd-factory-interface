package hci.project4.entities;

import java.util.ArrayList;

public class User {
	private String name;
	private String email;
	private String password;
	private boolean registered;
	private ArrayList<MusicCD> wishList;
	private ArrayList<MusicCD> viewed;
	
	public User(String name, String email, String password, boolean registered) {
		this.name = name;
		this.email = email;
		this.password = password;
		this.registered = registered;
		wishList = new ArrayList<MusicCD>();
		viewed = new ArrayList<MusicCD>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isRegistered() {
		return registered;
	}

	public void setRegistered(boolean registered) {
		this.registered = registered;
	}
	
	public ArrayList<MusicCD> getWishList() {
		return wishList;
	}
	
	public void addCDToWishList(MusicCD cd) {
		// additional security, to prevent exception
		if (!wishList.contains(cd)) {
			wishList.add(cd);
		}
	}
}
