package hci.project4.entities;

import hci.project4.utilities.XMLReader;

import java.util.ArrayList;

public class Catalog {
	private static Catalog instance = null;
	private ArrayList<MusicCD> cds;
	private final static String xmlPath = "src/project4.xml";
	
	public static Catalog getInstance() {
		if (instance == null) {
			instance = new Catalog(xmlPath);
		}
		
		return instance;
	}
	
	protected Catalog() {
		cds = new ArrayList<MusicCD>();
	}
	
	public Catalog(String xmlFilePath) {
		fillCatalog(xmlFilePath);
	}
	
	public void fillCatalog(String xmlFilePath) {
		cds = XMLReader.fillCDs(xmlFilePath);
	}
	
	public ArrayList<MusicCD> getCDs() {
		return cds;
	}
	
	public MusicCD getCD(String album, String artist) {
		for (int i = 0; i < cds.size(); i++) {
			if (cds.get(i).getAlbum().equals(album) && cds.get(i).getArtist().equals(artist)) {
				return cds.get(i);
			}
		}
		return null;
	}
	
	public ArrayList<String> getAllGenres() {
		ArrayList<String> genres = new ArrayList<String>();
		
		for (int i = 0; i < cds.size(); i++) {
			if (!genres.contains(cds.get(i).getGenre())) {
				genres.add(cds.get(i).getGenre());
			}
		}
		
		return genres;
	}
	
	public ArrayList<String> getArtistsByGenre(String genre) {
		ArrayList<String> artists = new ArrayList<String>();
		
		for (int i = 0; i < cds.size(); i++) {
			if (cds.get(i).getGenre().equals(genre)) {
				if (!artists.contains(cds.get(i).getArtist())) {
					artists.add(cds.get(i).getArtist());
				}
			}
		}
		
		return artists;
	}
	
	public ArrayList<String> getAlbumsByArtist(String artist) {
		ArrayList<String> albums = new ArrayList<String>();
		
		for (int i = 0; i < cds.size(); i++) {
			if (cds.get(i).getArtist().equals(artist)) {
				if (!albums.contains(cds.get(i).getAlbum())) {
					albums.add(cds.get(i).getAlbum());
				}
			}
		}
		
		return albums;
	}
}
