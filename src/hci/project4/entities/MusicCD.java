package hci.project4.entities;

public class MusicCD {
	private String artist;
	private String album;
	private String genre;
	/** cost in cents */
	private int cost;
	private int stock;
	private int bought;
	private int viewed;
	
	/* Constructors */
	
	public MusicCD() {
		artist = "";
		album = "";
		genre = "";
		cost = 150;
		stock = 0;
		bought = 0;
		viewed = 0;
	}
	
	public MusicCD(String album, String artist, String genre) {
		this.artist = artist;
		this.genre = genre;
		this.album = "";
		this.cost = 0;
		this.stock = 0;
		this.bought = 0;
		this.viewed = 0;
	}
	
	/* Getters and Setters */

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public String getAlbum() {
		return album;
	}

	public void setAlbum(String album) {
		this.album = album;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public int getBought() {
		return bought;
	}

	public void setBought(int bought) {
		this.bought = bought;
	}

	public int getViewed() {
		return viewed;
	}

	public void setViewed(int viewed) {
		this.viewed = viewed;
	}
	
	public void gotViewed() {
		this.viewed++;
	}
	
	public String getCostInDollars() {
		String cost = String.valueOf(this.cost);
		if (cost.length() == 1) {
			cost = "$0.0" + cost;
		} else if (cost.length() == 2) {
			cost = "$0." + cost;
		} else {
			String cents = "." + cost.substring(cost.length()-2);
			cost = "$" + cost.substring(0, cost.length()-cents.length()+1) + cents;
		}
		return cost;
	}
}
