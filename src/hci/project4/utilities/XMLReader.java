package hci.project4.utilities;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

import hci.project4.entities.MusicCD;

public class XMLReader {
	
	public static ArrayList<MusicCD> fillCDs(String path) {
		ArrayList<MusicCD> cds = new ArrayList<MusicCD>();
		
		try (Scanner scanner = new Scanner(Paths.get(path))) {
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				if (line.contains("<cd>")) {
					// New CD
					MusicCD cd = new MusicCD();
					line = scanner.nextLine();
					while (!line.contains("</cd>")) {
						line = line.trim();
						if (line.contains("<genre>")) {
							line = line.replace("<genre>", "");
							line = line.replace("</genre>", "");
							cd.setGenre(line);
						} else if (line.contains("<artist>")) {
							line = line.replace("<artist>", "");
							line = line.replace("</artist>", "");
							cd.setArtist(line);
						} else if (line.contains("<title>")) {
							line = line.replace("<title>", "");
							line = line.replace("</title>", "");
							cd.setAlbum(line);
						} else {
							System.err.println("XMLReader: Found unknown xml field '" + line + "'");
						}
						line = scanner.nextLine();
					}
					//End of CD
					cds.add(cd);
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return cds;
	}
}
