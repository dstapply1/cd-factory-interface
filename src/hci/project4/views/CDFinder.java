package hci.project4.views;

import hci.project4.entities.Catalog;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * @author dstapply
 *
 */
@SuppressWarnings("serial")
public class CDFinder extends JFrame implements ActionListener {
	private static JFrame CDFinderFrame;
	private JPanel mainPanel;
	private HomePanel homePanel = HomePanel.getInstance();
	private LoginPanel loginPanel = LoginPanel.getInstance();
	private RegisterPanel registerPanel = RegisterPanel.getInstance();
	private CDPanel cdPanel = CDPanel.getInstance();
	private WishListPanel wishListPanel = WishListPanel.getInstance();
	private BrowsePanel browsePanel = BrowsePanel.getInstance();
	
	private CDFinder() {
		mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.X_AXIS));
		
		loginPanel.setVisible(false);
		registerPanel.setVisible(false);
		cdPanel.setVisible(false);
		wishListPanel.setVisible(false);
		browsePanel.setVisible(false);
		
		//Order matters
		mainPanel.add(homePanel);
		mainPanel.add(loginPanel);
		mainPanel.add(registerPanel);
		mainPanel.add(cdPanel);
		mainPanel.add(wishListPanel);
		mainPanel.add(browsePanel);
		
		add(mainPanel);
	}
	
	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				initFrames();
			}
		});
	}
	
	private static void initFrames() {
		// Main Frame
		CDFinderFrame = new CDFinder();
		CDFinderFrame.setTitle("CD Finder");
		CDFinderFrame.setSize(1280, 768);
		CDFinderFrame.setLocation(200, 100);
		CDFinderFrame.setResizable(false);
		CDFinderFrame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		CDFinderFrame.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		
	}

}
