package hci.project4.views;

import hci.project4.entities.Catalog;
import hci.project4.entities.MusicCD;
import hci.project4.entities.UserController;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.Spring;
import javax.swing.SpringLayout;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

@SuppressWarnings("serial")
public class HomePanel extends JPanel implements ActionListener {
	private static HomePanel instance = null;
	private SpringLayout mainLayout;
	private BufferedImage logoImage;
	private JLabel logoLabel;
	private JLabel searchBarLabel, cdTableLabel;
	private JTextField searchBar;
	private JButton searchButton, loginButton, registerButton, browseButton;
	private JScrollPane tablePane;
	private JTable cdTable;
	private Vector<String> columnNames;
	private Vector<Vector<String>> tableData;
	private Catalog catalog = Catalog.getInstance();
	
	public static HomePanel getInstance() {
		if (instance == null) {
			instance = new HomePanel();
		}
		return instance;
	}
	
	private HomePanel() {
		super();
		tablePane = new JScrollPane();
		mainLayout = new SpringLayout();
		setLayout(mainLayout);
		
		Font labelFont = new Font("Verdana", Font.BOLD, 20);
		Font textFieldFont = new Font("Verdana", Font.PLAIN, 20);
		Font buttonFont = new Font("Verdana", Font.PLAIN, 18);
		Font tableFont = new Font("Verdana", Font.PLAIN, 16);
		Font tableLabelFont = new Font("Verdana", Font.PLAIN, 30);
		Font browseButtonFont = new Font("Verdana", Font.PLAIN, 32);
		
		//Logo
		try {
			logoImage = ImageIO.read(new File("src/images/smallLogo.png"));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		logoLabel = new JLabel(new ImageIcon(logoImage));
		SpringLayout.Constraints logoLabelCons = mainLayout.getConstraints(logoLabel);
		logoLabelCons.setX(Spring.constant(30));
		logoLabelCons.setY(Spring.constant(30));
		add(logoLabel);
		
		//Login/Register
		loginButton = new JButton("Log In");
		loginButton.setFont(buttonFont);
		loginButton.setActionCommand("login");
		loginButton.addActionListener(this);
		loginButton.setPreferredSize(new Dimension(110, 35));
		SpringLayout.Constraints loginButtonCons = mainLayout.getConstraints(loginButton);
		loginButtonCons.setX(Spring.constant(1100));
		loginButtonCons.setY(Spring.constant(50));
		add(loginButton);
		
		registerButton = new JButton("Register");
		registerButton.setFont(buttonFont);
		registerButton.setActionCommand("register");
		registerButton.addActionListener(this);
		registerButton.setPreferredSize(new Dimension(110, 35));
		SpringLayout.Constraints registerButtonCons = mainLayout.getConstraints(registerButton);
		registerButtonCons.setX(Spring.constant(1100));
		registerButtonCons.setY(Spring.constant(100));
		add(registerButton);
		
		
		// Search Bar
		searchBarLabel = new JLabel("Search for CD");
		searchBarLabel.setFont(labelFont);
		SpringLayout.Constraints searchBarLabelCons = mainLayout.getConstraints(searchBarLabel);
		searchBarLabelCons.setX(Spring.constant(360));
		searchBarLabelCons.setY(Spring.constant(180));
		add(searchBarLabel);
		
		searchBar = new JTextField(14);
		searchBar.setFont(textFieldFont);
		SpringLayout.Constraints searchBarCons = mainLayout.getConstraints(searchBar);
		searchBarCons.setX(Spring.constant(520));
		searchBarCons.setY(Spring.constant(178));
		add(searchBar);
		
		searchButton = new JButton("Search");
		searchButton.setFont(buttonFont);
		searchButton.setActionCommand("Search");
		searchButton.addActionListener(this);
		SpringLayout.Constraints searchCons = mainLayout.getConstraints(searchButton);
		searchCons.setX(Spring.constant(800));
		searchCons.setY(Spring.constant(178));
		add(searchButton);
		
		// Browse Button
		browseButton = new JButton("Browse");
		browseButton.setFont(browseButtonFont);
		browseButton.setActionCommand("browse");
		browseButton.addActionListener(this);
		browseButton.setPreferredSize(new Dimension(180, 90));
		SpringLayout.Constraints browseButtonCons = mainLayout.getConstraints(browseButton);
		browseButtonCons.setX(Spring.constant(50));
		browseButtonCons.setY(Spring.constant(200));
		add(browseButton);
		
		// Table Label
		cdTableLabel = new JLabel("Top 10 Popular CDs");
		cdTableLabel.setFont(tableLabelFont);
		SpringLayout.Constraints tableLabelCons = mainLayout.getConstraints(cdTableLabel);
		tableLabelCons.setX(Spring.constant(500));
		tableLabelCons.setY(Spring.constant(280));
		add(cdTableLabel);
		
		// CD Table
		columnNames = new Vector<String>();
		columnNames.add(0, "#");
		columnNames.add(1, "Album");
		columnNames.add(2, "Artist");
		columnNames.add(3, "Genre");
		columnNames.add(4, "Cost");
		
		tableData = new Vector<Vector<String>>();
		
		DefaultTableModel tableModel = new DefaultTableModel(tableData, columnNames) {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		
		cdTable = new JTable(tableModel);
		cdTable.getColumnModel().getColumn(0).setMaxWidth(50);
		cdTable.getColumnModel().getColumn(1).setMinWidth(230);
		cdTable.getColumnModel().getColumn(2).setMinWidth(150);
		cdTable.getColumnModel().getColumn(4).setMaxWidth(80);
		cdTable.getColumnModel().getColumn(4).setMinWidth(80);
		cdTable.setFont(tableFont);
		cdTable.setRowHeight(30);
		
		for (int i = 0; i < 10; i++) {
			//TODO sort top 10 by popularity
			MusicCD cd = catalog.getCDs().get(i);
			Vector<String> row = new Vector<String>();
			row.add(0, String.valueOf(i+1));
			row.add(1, cd.getAlbum());
			row.add(2, cd.getArtist());
			row.add(3, cd.getGenre());
			row.add(4, cd.getCostInDollars());
			
			tableData.add(row);
		}
		
		cdTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		cdTable.addMouseListener(new MouseAdapter() {
		    public void mousePressed(MouseEvent me) {
		        JTable table =(JTable) me.getSource();
		        Point p = me.getPoint();
		        int row = table.rowAtPoint(p);
		        if (me.getClickCount() == 2) {
		        	MusicCD cd;
		        	if ((cd = catalog.getCD((String)table.getValueAt(row, 1), (String)table.getValueAt(row, 2))) != null) {
		        		goToCDPanel(cd);
		        	} else {
		        		//cd not found
		        	}
		        }
		    }
		});
		
		tablePane.setPreferredSize(new Dimension(800, 323));
		
		SpringLayout.Constraints cdTableCons = mainLayout.getConstraints(tablePane);
		cdTableCons.setX(Spring.constant(260));
		cdTableCons.setY(Spring.constant(325));
		tablePane.getViewport().add(cdTable);
		
		add(tablePane);
	}
	
	private void goToCDPanel(MusicCD cd) {
		this.setVisible(false);
		CDPanel.getInstance().setCD(cd);
		CDPanel.getInstance().setVisible(true);
	}
	
	public void setLoggedIn(boolean value) {
		if (value) {
			loginButton.setText("Log Out");
			loginButton.setActionCommand("logout");
			registerButton.setText("Wish List");
			registerButton.setActionCommand("wishlist");
			registerButton.setPreferredSize(new Dimension(150, 35));
		} else {
			loginButton.setText("Log In");
			loginButton.setActionCommand("login");
			registerButton.setText("Register");
			registerButton.setActionCommand("register");
			registerButton.setPreferredSize(new Dimension(110, 35));
		}
	}

	@Override
	public void actionPerformed(ActionEvent action) {
		if (action.getActionCommand().equals("login")) {
			this.setVisible(false);
			this.getParent().getComponent(1).setVisible(true);
		} else if (action.getActionCommand().equals("register")) {
			this.setVisible(false);
			this.getParent().getComponent(2).setVisible(true);
		} else if (action.getActionCommand().equals("logout")) {
			UserController.getInstance().unLogUser();
			CDPanel.getInstance().setAddToWishListButtonVisible(false);
			setLoggedIn(false);
		} else if (action.getActionCommand().equals("wishlist")) {
			this.setVisible(false);
			this.getParent().getComponent(4).setVisible(true);
		} else if (action.getActionCommand().equals("browse")) {
			BrowsePanel.getInstance().goToBrowseState(BrowseState.GENRE_STATE, "");
			this.setVisible(false);
			this.getParent().getComponent(5).setVisible(true);
		}
	}
}
